import React, { Component } from 'react'

class Landing extends Component {
  render() {
    //Boostrap
    //container_容器
    //jumbotron 圓角
    //col-sm-8  分八個欄位 mx-auto空白移到兩邊
    return (
      <div className="container">
        <div className="jumbotron mt-5">
          <div className="col-sm-8 mx-auto">
            <h1 className="text-center">I will try my best </h1>
          </div>
        </div>
      </div>
    )
  }
}

export default Landing