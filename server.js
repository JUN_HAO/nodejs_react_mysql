var express = require('express')
//載入 express 函式庫
var cors = require('cors')
//Express 預設就會是 mode: no-cors 非同網域不能獲取資源
var bodyParser = require('body-parser')
//作用是對post请求的請求進行解析
var app = express()
//建立一個Express伺服器
var port = process.env.PORT || 5000
//環境變量PORT中的任何內容，如果沒有任何內容，則為3000。

app.use(bodyParser.json())
app.use(cors())
app.use(
  bodyParser.urlencoded({
    extended: false
  })
)

var Users = require('./routes/Users')

app.use('/users', Users)

//所有/users的請求都會經過./routes/Users的處理

app.listen(port, function() {
  console.log('Server is running on port: ' + port)
})